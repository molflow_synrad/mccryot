/*
  File:        TextureSettings.cpp
  Description: Texture settings dialog (min,max,autoscale,gradient)
  Program:     MolFlow
  Author:      R. KERSEVAN / J-L PONS / M SZAKACS
  Copyright:   E.S.R.F / CERN

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "TextureSettings.h"
#include "GLApp/GLToolkit.h"
#include "GLApp/GLMessageBox.h"
extern double totalOutgassing;
extern double gasMass;

// --------------------------------------------------------------------

TextureSettings::TextureSettings():GLWindow() {

  int wD = 450;
  int hD = 195;

  SetTitle("Texture Scaling");
  SetIconfiable(TRUE);

  GLTitledPanel *panel = new GLTitledPanel("Texture Range");
  panel->SetBounds(5,2,315,98);
  Add(panel);

  GLLabel *l1 = new GLLabel("Min/Max");
  l1->SetBounds(10,20,50,18);
  Add(l1);

  texMinMaxText = new GLTextField(0,"");
  texMinMaxText->SetBounds(60,20,85,19);
  texMinMaxText->SetEditable(TRUE);
  Add(texMinMaxText);

  /*GLLabel *l2 = new GLLabel("Max");
  l2->SetBounds(10,45,50,18);
  Add(l2);

  texMaxText = new GLTextField(0,"");
  texMaxText->SetBounds(60,45,85,19);
  texMaxText->SetEditable(TRUE);
  Add(texMaxText);*/

  setCurrentButton = new GLButton(0,"Set to current");
  setCurrentButton->SetBounds(105,70,90,19);
  Add(setCurrentButton);

  texAutoScale = new GLToggle(0,"Autoscale");
  texAutoScale->SetBounds(150,20,80,19);
  Add(texAutoScale);

  /*colormapBtn = new GLToggle(0,"Use colormap");
  colormapBtn->SetBounds(230,20,85,19);
  Add(colormapBtn);*/

  logBtn = new GLToggle(0,"Log scale");
  logBtn->SetBounds(150,45,80,19);
  Add(logBtn);

  GLLabel *l3 = new GLLabel("Swap");
  l3->SetBounds(230,45,30,18);
  Add(l3);

  swapText = new GLTextField(0,"");
  swapText->SetEditable(FALSE);
  swapText->SetBounds(260,45,50,18);
  Add(swapText);

  // ---------------------------------------------------

  GLTitledPanel *panel2 = new GLTitledPanel("Current");
  panel2->SetBounds(325,2,120,98);
  Add(panel2);

  GLLabel *l4 = new GLLabel("Min:");
  l4->SetBounds(340,30,20,19);
  Add(l4);

  texCMinText = new GLLabel("");
  texCMinText->SetBounds(370,30,70,19);
  Add(texCMinText);

  GLLabel *l5 = new GLLabel("Max:");
  l5->SetBounds(340,65,20,19);
  Add(l5);

  texCMaxText = new GLLabel("");
  texCMaxText->SetBounds(370,65,70,19);
  Add(texCMaxText);

  // ---------------------------------------------------

  GLTitledPanel *panel3 = new GLTitledPanel("Gradient");
  panel3->SetBounds(5,102,440,65);
  Add(panel3);

  gradient = new GLGradient(0);
  gradient->SetMouseCursor(TRUE);
  gradient->SetBounds(10,117,420,40);
  Add(gradient);

  // ---------------------------------------------------

  updateButton = new GLButton(0,"Apply");
  updateButton->SetBounds(10,70,90,19);
  Add(updateButton);

  // Center dialog
  int wS,hS;
  GLToolkit::GetScreenSize(&wS,&hS);
  int xD = (wS-wD)/2;
  int yD = (hS-hD)/2;
  SetBounds(xD,yD,wD,hD);

  RestoreDeviceObjects();

  geom = NULL;

}

// --------------------------------------------------------------------

void TextureSettings::UpdateSize() {

  DWORD swap = 0;
  int nbFacet = geom->GetNbFacet();
  for(int i=0;i<nbFacet;i++) {
    Facet *f = geom->GetFacet(i);
    if(f->sh.isTextured) {
      //swap += f->GetTexSwapSize(colormapBtn->IsChecked());
		swap += f->GetTexSwapSize(TRUE);
    }
  }
  swapText->SetText(FormatMemory(swap));

}

// --------------------------------------------------------------------

void TextureSettings::Update() {

  if(!IsVisible() || IsIconic()) return;  

  char tmp[128];
  sprintf(tmp,"%.3E",geom->texCMin);
  texCMinText->SetText(tmp);
  sprintf(tmp,"%.3E",geom->texCMax);
  texCMaxText->SetText(tmp);
  texAutoScale->SetCheck(geom->texAutoScale);
  logBtn->SetCheck(geom->texLogScale);
  gradient->SetScale(geom->texLogScale?LOG_SCALE:LINEAR_SCALE);
  if( !geom->texAutoScale ) {
    gradient->SetMinMax(geom->texCMin,geom->texMax);
  } else {
    gradient->SetMinMax(geom->texCMin,geom->texCMax);
  }
  if (!geom->texLogScale) {
	  gradient->SetType(GRADIENT_COLOR);
  } else {
	  gradient->SetType(GRADIENT_BW);
  }
  UpdateSize();

}

// --------------------------------------------------------------------

void TextureSettings::Display(Worker *w,GeometryViewer **v) {

  worker = w;
  geom = w->GetGeometry();
  viewers = v;
  if(!geom->IsLoaded()) {
	  GLMessageBox::Display("No geometry loaded.","No geometry",GLDLG_OK,GLDLG_ICONERROR);
	  return;
  }
  char tmp[128];
  sprintf(tmp,"%.3E",MAX(abs(geom->texMin),abs(geom->texMax)));
  texMinMaxText->SetText(tmp);

  SetVisible(TRUE);
  Update();

}

// --------------------------------------------------------------------

void TextureSettings::ProcessMessage(GLComponent *src,int message) {

  switch(message) {
    case MSG_BUTTON:

    if (src==updateButton) {

      double range;

      if( !texMinMaxText->GetNumber(&range) ) {
        GLMessageBox::Display("Invalid minimum value","Error",GLDLG_OK,GLDLG_ICONERROR);
        Update();
        return;
      }
      /*if( !texMaxText->GetNumber(&max) ) {
        GLMessageBox::Display("Invalid maximum value","Error",GLDLG_OK,GLDLG_ICONERROR);
        Update();
        return;
      }*/
      if( range==0.0 ) {
        GLMessageBox::Display("Texture range must be positive","Error",GLDLG_OK,GLDLG_ICONERROR);
        Update();
        return;
      }
      geom->texMin = 0;
      geom->texMax = abs((AHIT)range);
      geom->texAutoScale = texAutoScale->IsChecked();
      worker->Update(0.0f);
      Update();

    } else if (src==setCurrentButton) {
		geom->texMin=0;
		geom->texMax=geom->texCMax;
		char tmp[128];
		sprintf(tmp,"%g",MAX(abs(geom->texMin),abs(geom->texMax)));
		texMinMaxText->SetText(tmp);
		//texMaxText->SetText(texCMaxText->GetText());
		texAutoScale->SetCheck(FALSE);
		geom->texAutoScale=false;
		worker->Update(0.0f);
		Update();
	}
    break;

    case MSG_TOGGLE:
    /*if (src==colormapBtn) {
      for(int i=0;i<MAX_VIEWER;i++) viewers[i]->showColormap = colormapBtn->IsChecked();
      geom->texColormap = colormapBtn->IsChecked();
      worker->Update(0.0f);
      Update();
    } else*/ if (src==texAutoScale) {
      geom->texAutoScale = texAutoScale->IsChecked();
      worker->Update(0.0f);
      Update();
    } else if (src==logBtn) {
      geom->texLogScale = logBtn->IsChecked();
      gradient->SetScale(geom->texLogScale?LOG_SCALE:LINEAR_SCALE);
      worker->Update(0.0f);
      Update();
    }
    break;

    case MSG_TEXT:
    ProcessMessage(updateButton,MSG_BUTTON);
    break;
  }

  GLWindow::ProcessMessage(src,message);
}
