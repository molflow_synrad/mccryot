#pragma once
namespace molflow_updater {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{

			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected: 
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::LinkLabel^  linkLabel1;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ProgressBar^  progressBar1;
	private: System::Windows::Forms::RichTextBox^  status;
	private: System::Windows::Forms::Button^  button2;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::LinkLabel^  linkLabel2;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->status = (gcnew System::Windows::Forms::RichTextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->linkLabel2 = (gcnew System::Windows::Forms::LinkLabel());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Enabled = false;
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(12, 56);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(281, 28);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Checking updates...";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(9, 4);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(113, 23);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Latest version:";
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Location = System::Drawing::Point(110, 27);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(76, 17);
			this->linkLabel1->TabIndex = 2;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"checking...";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Form1::linkLabel1_LinkClicked);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(110, 4);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(76, 17);
			this->label2->TabIndex = 4;
			this->label2->Text = L"checking...";
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(12, 90);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(822, 22);
			this->progressBar1->TabIndex = 5;
			// 
			// status
			// 
			this->status->Location = System::Drawing::Point(12, 118);
			this->status->Name = L"status";
			this->status->ReadOnly = true;
			this->status->Size = System::Drawing::Size(822, 377);
			this->status->TabIndex = 6;
			this->status->Text = L"";
			this->status->TextChanged += gcnew System::EventHandler(this, &Form1::status_TextChanged);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(557, 56);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(271, 28);
			this->button2->TabIndex = 7;
			this->button2->Text = L"Don\'t notify again for this version";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// label3
			// 
			this->label3->Location = System::Drawing::Point(9, 27);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(95, 23);
			this->label3->TabIndex = 9;
			this->label3->Text = L"URL:";
			// 
			// linkLabel2
			// 
			this->linkLabel2->AutoSize = true;
			this->linkLabel2->Location = System::Drawing::Point(381, 63);
			this->linkLabel2->Name = L"linkLabel2";
			this->linkLabel2->Size = System::Drawing::Size(107, 17);
			this->linkLabel2->TabIndex = 10;
			this->linkLabel2->TabStop = true;
			this->linkLabel2->Text = L"View changelog";
			this->linkLabel2->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Form1::linkLabel2_LinkClicked);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::InactiveBorder;
			this->ClientSize = System::Drawing::Size(840, 507);
			this->Controls->Add(this->linkLabel2);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->status);
			this->Controls->Add(this->progressBar1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"Form1";
			this->Text = L"Checking for McCryoT+ updates...";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		String^ dirname;
		String^ currentdir;
		int currentver,newver;
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				 button1->Enabled=false;
				 if (button1->Text == "Download update") {
					 status->Text+="\r\nDownloading new McCryoT...";
					 try {
						 System::Net::WebClient ^_WebClient2 = gcnew System::Net::WebClient();
						 _WebClient2->DownloadFileCompleted += gcnew AsyncCompletedEventHandler(this, &Form1::_DownloadFileCompleted2);
						 _WebClient2->DownloadProgressChanged += gcnew System::Net::DownloadProgressChangedEventHandler(this, &Form1::_DownloadProgressChanged2);
						 _WebClient2->DownloadFileAsync(gcnew Uri(linkLabel1->Text), "update.zip");
					 } catch (Exception^ _Exception) {
						 status->Text += "\r\nError: {0}",_Exception->ToString();
					 }
				 }
				 else if (button1->Text == "Apply update") {
					 System::Windows::Forms::MessageBox::Show( L"Please close all instances of Molfow (if they are still running)", L"Prepare to update", 
						 System::Windows::Forms::MessageBoxButtons::OK);
					 System::String ^_Output1 = nullptr;

					 System::String ^_Error = nullptr;
					 try {

						 //status->Text+="\r\nDone. Now making backup of old version...\r\n"+_Output1;this->Refresh();
						 //ExecuteShellCommand("xcopy", "*.* c:\oldversion /i /e /y", _Output2, _Error);
						 //status->Text+="\r\n"+_Output2;
						 status->Text+="\r\nCopying extracted files to place...";
						 status->Select(status->TextLength-1,0);status->ScrollToCaret();
						 System::Windows::Forms::Application::DoEvents();
						 ExecuteShellCommand("xcopy", "\""+currentdir+"\\"+dirname+"\" \""+currentdir+"\" /e /i /y", _Output1, _Error);
					 } catch(Exception^ exc) {
						 status->Text+="\r\nException: "+exc->ToString();
						 status->Select(status->TextLength-1,0);status->ScrollToCaret();
						 System::Windows::Forms::Application::DoEvents();
					 }
					 status->Text+="\r\n"+_Error+_Output1;
					 status->Text+="\r\nDone copying.";
					 status->Select(status->TextLength-1,0);status->ScrollToCaret();
					 System::Windows::Forms::Application::DoEvents();
					 status->Text+="\r\nDeleting extracted files...";
					 status->Select(status->TextLength-1,0);status->ScrollToCaret();
					 System::Windows::Forms::Application::DoEvents();
					 ExecuteShellCommand("rmdir", "\""+currentdir+"\\"+dirname+"\" /s /q", _Output1, _Error);
					 /*status->Text+="\r\nUpdating currentversion.txt";
					 status->Select(status->TextLength-1,0);status->ScrollToCaret();
					 System::Windows::Forms::Application::DoEvents();
					 System::IO::StreamWriter^ din = gcnew System::IO::StreamWriter("currentversion.txt");
					 din->WriteLine(newver);
					 din->Close();*/
					 status->Text+="\r\nCongratulations! Now you have version #"+newver+" ("+dirname+").";
					 status->Select(status->TextLength-1,0);status->ScrollToCaret();
					 button1->Text="Run new McCryoT!";
					 button1->Enabled=true;
				 } else if (button1->Text=="Run new McCryoT!") {
					 System::String ^_Output2 = nullptr;
					 System::String ^_Error = nullptr;
					 //ExecuteShellCommand_dontwait("mccryot.exe", "", _Output2, _Error);
					 System::Diagnostics::Process^ proc = gcnew System::Diagnostics::Process();
					 proc->EnableRaisingEvents=false;
					 //if (System::IO::File::Exists(currentdir+"\\mccryot.exe")) {
					 if (System::IO::File::Exists("mccryot.exe")) {
					 //proc->StartInfo->FileName="\""+currentdir+"\\mccryot.exe\"";
					 proc->StartInfo->FileName="mccryot.exe";
					 proc->Start();
					 this->Close();
					 } else {
						 status->Text+="\r\nError: mccryot.exe not found!";
					 }
				 }
			 }

	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 //using System;
				 //using System.Net;
				 //using System.IO;
				 currentdir = System::IO::Directory::GetCurrentDirectory();
				 button1->Enabled = false;
				 status->Text+="McCryoT directory: "+currentdir;
				 status->Text+="\r\nChecking for latest version... (timeout: 15 sec)";
				 this->Visible=true;
				 System::Windows::Forms::Application::DoEvents();
				 String^ infoFile = DownloadHTMLPage("http://test-molflow.web.cern.ch/sites/test-molflow.web.cern.ch/files/autoupdate_mccryot.txt"); //update information
				 if (infoFile==nullptr) {
					 status->Text+="\r\nNo data from server.";
					 return;
				 }
				 status->Text+="\r\nVersion number downloaded.";
				 System::Windows::Forms::Application::DoEvents();
				 DownloadHTMLPage("http://goo.gl/AO7HQ"); //visitor statistics
				 DownloadHTMLPage("http://test-molflow.web.cern.ch/sites/test-molflow.web.cern.ch/files/update.php");
				 // File download completed
				 //download_button->Enabled = Enabled;
				 status->Text+="\r\nVersion information downloaded.";
				 System::Windows::Forms::Application::DoEvents();
				 //if (!System::IO::File::Exists(currentdir+"\\currentversion.txt")) {
				 if (!System::IO::File::Exists("currentversion.txt")) {
					 status->Text+="\r\ncurrentversion.txt file not found. Setting version to #0.";
					 System::IO::StreamWriter^ din5 = gcnew System::IO::StreamWriter("currentversion.txt");
					 din5->WriteLine(newver);
					 din5->Close();
				 }
				//System::IO::StreamReader^ din = System::IO::File::OpenText(currentdir+"\\currentversion.txt");
				System::IO::StreamReader^ din = System::IO::File::OpenText("currentversion.txt");
				 String^ str = din->ReadLine();
				 currentver = Int32::Parse(str);
				 status->Text+="\r\nCurrent version is: #"+currentver;
				 System::Windows::Forms::Application::DoEvents();
				 din->Close();

				 System::IO::StringReader^ din2 = gcnew System::IO::StringReader(infoFile);
				 str = din2->ReadLine();
				 newver = Int32::Parse(str);
				 linkLabel1->Text=din2->ReadLine();
				 dirname = din2->ReadLine();
				 din2->Close();
				 label2->Text = "#" + newver+" ("+dirname+")";
				 status->Text+="\r\nLatest version is: #"+newver+" ("+dirname+").";
				 status->Text+="\r\nFile URL: "+linkLabel1->Text;

				 if (newver>currentver) {
					 button1->Text="Download update";
					 button1->Enabled = true;
					 this->Text = "A new version of McCryoT is available!";
					 WindowState = FormWindowState::Normal;
				 }
				 else {
					 this->Text = "You have the latest McCryoT version.";
					 status->Text+="\r\nYou have the latest version. Exiting in 5 seconds.";
					 button1->Text = "Exiting.";
					 System::Windows::Forms::Application::DoEvents();
					 System::Threading::Thread::Sleep(5000);
					 this->Close();
				 }
			 }

			 void Form1::_DownloadFileCompleted2(System::Object ^sender, AsyncCompletedEventArgs ^e)
			 {
				 status->Text+="\r\nMoflow update downloaded.";
				 System::String ^_Output2 = nullptr;
				 System::String ^_Error = nullptr;
				 if (!System::IO::File::Exists("7za.exe")) {
					 status->Text+="\r\nError: 7za.exe not found. Decompressing impossible. Trying to download it...";
					 status->Select(status->TextLength-1,0);
					 status->ScrollToCaret();
					 System::Windows::Forms::Application::DoEvents();
					 DownloadFile("http://test-molflow.web.cern.ch/sites/test-molflow.web.cern.ch/files/7za.exe","7za.exe");
				 }
				 if (!System::IO::File::Exists("7za.exe")) {
					 status->Text+="\r\nError: Couldn't download it. Giving up.";
					 return;
				 }
				 try {
					 //check
					 ExecuteShellCommand("pushd \""+currentdir+" \"&&7za.exe", "x -tzip \""+currentdir+"\\update.zip\" -y -o\""+currentdir+"\"&&popd", _Output2, _Error);
					 status->Text+="\r\n"+_Error+_Output2;this->Refresh();
					 //check if extraction was ok
					 ExecuteShellCommand("del", "\""+currentdir+"\\update.zip\"", _Output2, _Error);
				 } catch(Exception^ exc) {
					 status->Text+="\r\nException: "+exc->ToString();
				 }
				 status->Text+="\r\n"+_Error+_Output2 + "\r\nExtraction complete. You can now apply the update";
				 status->Select(status->TextLength-1,0);
				 status->ScrollToCaret();
				 System::Windows::Forms::Application::DoEvents();
				 button1->Text = "Apply update";
				 button1->Enabled = true;
				 this->Refresh();


			 }
			 // Occurs when an asynchronous download operation successfully transfers some or all of the data.
			 void Form1::_DownloadProgressChanged2(System::Object ^sender, System::Net::DownloadProgressChangedEventArgs ^e)
			 {
				 // Update progress bar
				 progressBar1->Value = e->ProgressPercentage;
			 }
	private: System::Void linkLabel1_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
				 System::Diagnostics::Process::Start(linkLabel1->Text);
			 }

			 /// <summary>
			 /// Execute a shell command
			 /// </summary>
			 /// Source: http://www.digitalcoding.com/Code-Snippets/CPP-CLI/C-CLI-Code-Snippet-Execute-Shell-Commands-from-Net.html
			 /// <param name="_FileToExecute">File/Command to execute</param>
			 /// <param name="_CommandLine">Command line parameters to pass</param> 
			 /// <param name="_outputMessage">returned string value after executing shell command</param> 
			 /// <param name="_errorMessage">Error messages generated during shell execution</param> 
			 void ExecuteShellCommand(System::String ^_FileToExecute, System::String ^_CommandLine, System::String ^%_outputMessage, System::String ^%_errorMessage)
			 {
				 // Set process variable
				 // Provides access to local and remote processes and enables you to start and stop local system processes.
				 System::Diagnostics::Process ^_Process = nullptr;
				 try
				 {
					 _Process = gcnew System::Diagnostics::Process();

					 // invokes the cmd process specifying the command to be executed.
					 System::String ^_CMDProcess = System::String::Format(System::Globalization::CultureInfo::InvariantCulture, "{0}\\cmd.exe", gcnew array<System::Object^> { Environment::SystemDirectory });

					 // pass executing file to cmd (Windows command interpreter) as a arguments
					 // /C tells cmd that we want it to execute the command that follows, and then exit.
					 System::String ^_Arguments = System::String::Format(System::Globalization::CultureInfo::InvariantCulture, "/C {0}", gcnew array<System::Object^> { _FileToExecute });

					 // pass any command line parameters for execution
					 if (_CommandLine != nullptr && _CommandLine->Length > 0)
					 {
						 _Arguments += System::String::Format(System::Globalization::CultureInfo::InvariantCulture, " {0}", gcnew array<System::Object^> { _CommandLine, System::Globalization::CultureInfo::InvariantCulture });
					 }

					 // Specifies a set of values used when starting a process.
					 System::Diagnostics::ProcessStartInfo ^_ProcessStartInfo = gcnew System::Diagnostics::ProcessStartInfo(_CMDProcess, _Arguments);
					 // sets a value indicating not to start the process in a new window. 
					 _ProcessStartInfo->CreateNoWindow = true;
					 // sets a value indicating not to use the operating system shell to start the process. 
					 _ProcessStartInfo->UseShellExecute = false;
					 // sets a value that indicates the output/input/error of an application is written to the Process.
					 _ProcessStartInfo->RedirectStandardOutput = true;
					 _ProcessStartInfo->RedirectStandardInput = true;
					 _ProcessStartInfo->RedirectStandardError = true;
					 _Process->StartInfo = _ProcessStartInfo;

					 // Starts a process resource and associates it with a Process component.
					 _Process->Start();

					 // Instructs the Process component to wait indefinitely for the associated process to exit.
					 _errorMessage = _Process->StandardError->ReadToEnd();
					 _Process->WaitForExit();

					 // Instructs the Process component to wait indefinitely for the associated process to exit.
					 _outputMessage = _Process->StandardOutput->ReadToEnd();
					 _Process->WaitForExit();
				 }
				 catch (Win32Exception ^_Win32Exception)
				 {
					 // Error
					 status->Text+="\r\nWin32 Exception caught in process: "+ _Win32Exception->ToString();
				 }
				 catch (Exception ^_Exception)
				 {
					 // Error
					 status->Text+="\r\nException caught in process: "+ _Exception->ToString();
				 }
				 finally
				 {
					 // close process and do cleanup
					 _Process->Close();
					 delete _Process;
					 _Process = nullptr;
				 }
			 }

			 void ExecuteShellCommand_dontwait(System::String ^_FileToExecute, System::String ^_CommandLine, System::String ^%_outputMessage, System::String ^%_errorMessage)
			 {
				 // Set process variable
				 // Provides access to local and remote processes and enables you to start and stop local system processes.
				 System::Diagnostics::Process ^_Process = nullptr;
				 try
				 {
					 _Process = gcnew System::Diagnostics::Process();

					 // invokes the cmd process specifying the command to be executed.
					 System::String ^_CMDProcess = System::String::Format(System::Globalization::CultureInfo::InvariantCulture, "{0}\\cmd.exe", gcnew array<System::Object^> { Environment::SystemDirectory });

					 // pass executing file to cmd (Windows command interpreter) as a arguments
					 // /C tells cmd that we want it to execute the command that follows, and then exit.
					 System::String ^_Arguments = System::String::Format(System::Globalization::CultureInfo::InvariantCulture, "/C {0}", gcnew array<System::Object^> { _FileToExecute });

					 // pass any command line parameters for execution
					 if (_CommandLine != nullptr && _CommandLine->Length > 0)
					 {
						 _Arguments += System::String::Format(System::Globalization::CultureInfo::InvariantCulture, " {0}", gcnew array<System::Object^> { _CommandLine, System::Globalization::CultureInfo::InvariantCulture });
					 }

					 // Specifies a set of values used when starting a process.
					 System::Diagnostics::ProcessStartInfo ^_ProcessStartInfo = gcnew System::Diagnostics::ProcessStartInfo(_CMDProcess, _Arguments);
					 // sets a value indicating not to start the process in a new window. 
					 _ProcessStartInfo->CreateNoWindow = true;
					 // sets a value indicating not to use the operating system shell to start the process. 
					 _ProcessStartInfo->UseShellExecute = true;
					 // sets a value that indicates the output/input/error of an application is written to the Process.
					 _ProcessStartInfo->RedirectStandardOutput = true;
					 _ProcessStartInfo->RedirectStandardInput = true;
					 _ProcessStartInfo->RedirectStandardError = true;
					 _Process->StartInfo = _ProcessStartInfo;

					 // Starts a process resource and associates it with a Process component.
					 _Process->Start();

					 // Instructs the Process component to wait indefinitely for the associated process to exit.
					 _errorMessage = _Process->StandardError->ReadToEnd();
					 //_Process->WaitForExit();

					 // Instructs the Process component to wait indefinitely for the associated process to exit.
					 _outputMessage = _Process->StandardOutput->ReadToEnd();
					 // _Process->WaitForExit();
				 }
				 catch (Win32Exception ^_Win32Exception)
				 {
					 // Error
					 status->Text+="\r\nWin32 Exception caught in process: "+ _Win32Exception->ToString();
				 }
				 catch (Exception ^_Exception)
				 {
					 // Error
					 status->Text+="\r\nException caught in process: "+ _Exception->ToString();
				 }
				 finally
				 {
					 // close process and do cleanup
					 _Process->Close();
					 delete _Process;
					 _Process = nullptr;
				 }
			 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
				 System::IO::StreamWriter^ din = gcnew System::IO::StreamWriter("currentversion.txt");
				 din->WriteLine(newver);
				 din->Close();
				 this->Close();
			 }
	/*private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
				 System::IO::StreamWriter^ din = gcnew System::IO::StreamWriter("currentversion.txt");
				 din->WriteLine(99999);
				 din->Close();
				 this->Close();
			 }*/

			 /// <summary>
			 /// Function to download HTML web page from URL
			 /// </summary>
			 /// <param name="_URL">URL address to download web page</param>
			 /// <returns>HTML contents as a string</returns>
			 System::String ^DownloadHTMLPage(System::String ^_URL)
			 {
				 System::String ^_PageContent = nullptr;
				 try
				 {
					 // Open a connection
					 System::Net::HttpWebRequest ^_HttpWebRequest = safe_cast<System::Net::HttpWebRequest^>(System::Net::HttpWebRequest::Create(_URL));

					 // You can also specify additional header values like the user agent or the referer: (Optional)
					 _HttpWebRequest->UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)";
					 _HttpWebRequest->Referer = "http://www.google.com/";

					 // set timeout for 10 seconds (Optional)
					 _HttpWebRequest->Timeout = 15000;

					 // Request response:
					 System::Net::WebResponse ^_WebResponse = _HttpWebRequest->GetResponse();

					 // Open data stream:
					 System::IO::Stream ^_WebStream = _WebResponse->GetResponseStream();

					 // Create reader object:
					 System::IO::StreamReader ^_StreamReader = gcnew System::IO::StreamReader(_WebStream);

					 // Read the entire stream content:
					 _PageContent = _StreamReader->ReadToEnd();

					 // Cleanup
					 _StreamReader->Close();
					 _WebStream->Close();
					 _WebResponse->Close();
				 }
				 catch (Exception ^_Exception)
				 {
					 // Error
					 status->Text += "\r\nException caught in process: "+ _Exception->ToString();
					 return nullptr;
				 }

				 return _PageContent;
			 }

			 /// <summary>
/// Function to download a file from URL and save it to local drive
/// </summary>
/// <param name="_URL">URL address to download file</param>
void DownloadFile(System::String ^_URL, System::String ^_SaveAs)
{
    try
    {
        System::Net::WebClient ^_WebClient = gcnew System::Net::WebClient();
        // Downloads the resource with the specified URI to a local file.
        _WebClient->DownloadFile(_URL, _SaveAs);
    }
    catch (Exception ^_Exception)
    {
        // Error
        Console::WriteLine("Exception caught in process: {0}", _Exception->ToString());
    }
}

	private: System::Void linkLabel2_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
				 System::Diagnostics::Process::Start("http://test-molflow.web.cern.ch/tags/changelog");
			 }
private: System::Void status_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}

